# Archlinux Buildserver Tools

This Repository contains Tools to automatically build packages from the AUR with cronjobs.

## Fill the lists
change the `packages/{hourly,daily,weekly}` files to what you want to have build regularly.

## Dependencies

```txt
docker
zstd
gpg
git
nginx
```

## Setup

To setup you'll have to create a key with gpg to sign packages build and uploaded to your webroot.

To create a key:
```sh
    gpg --full-generate-key
```
List your private keys:

```sh
    gpg -K
```

Export the private key into the cloned repository folder with:
```sh
    KEY_ID=${KEY_ID:-"ABCD1234"}
    gpg --export-secret-keys --armor $KEY_ID > signing.asc
    #sends the public key to default keyservers
    gpg --send-keys --armor $KEY_ID
```

## Create the cronjobs
To automatically run the tasks create cronjobs e.g. this will invoke the corresponding package list to be rebuild if new versions are available.
```crontab
REPO_PATH = /path/to/repo
@hourly bash -c 'REPO_NAME=MY_REPONAME $REPO_PATH/cron.sh hourly'
@daily bash -c 'REPO_NAME=MY_REPONAME $REPO_PATH/cron.sh daily'
@weekly bash -c 'REPO_NAME=MY_REPONAME $REPO_PATH/cron.sh weekly'
```

## Setup NGINX as host
You'll have to change the values to your needs and enable SSL with e.g. letsencrypt.

Create a link from the build directory to a web location:

```sh
    ls -s /path/to/repo/build /var/www/builds
```

```nginx
server {
    listen 80;
    listen [::]:80;
    server_name my.doma.in;
    root /var/www/builds;
    
    location /YOUR-REPONAME/x86_64 {
		return 302 http://$host$uri/;
	}
	
	location /YOUR-REPONAME/x86_64/ {
		autoindex on;
		alias /var/www/builds/;
	}
}
```

And reload nginx `systemctl reload nginx`

## Setup your local repository on your PC

Search your keyid on the keyserver you submitted earlier and add the key to your keyring e.g.

```sh
    sudo pacman-key --recv-key <YOUR-KEY-ID> --keyserver keyserver.ubuntu.com
    sudo pacman-key --lsign-key <YOUR-KEY-ID>
```

Add these lines to your `/etc/pacman.conf`

```conf
[YOUR-REPONAME]
Include = /etc/pacman.d/YOUR-MIRRORLIST
```

Add these lines to your `/etc/pacman.d/YOUR-MIRRORLIST`
```conf
Server = https://your.doma.in/$repo/$arch
```

Update your mirrors and you are ready to go.