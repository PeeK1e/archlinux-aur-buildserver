FROM archlinux:base-devel AS git
RUN pacman -Syu git --needed --noconfirm

FROM git AS prepare
WORKDIR /tmp
RUN git clone https://aur.archlinux.org/paru-bin.git . && \
    ls -ahl . && \
    source $(pwd)/PKGBUILD && \
    pacman -S --noconfirm --needed --asdeps "${makedepends[@]}" "${depends[@]}"

FROM prepare AS build
USER nobody
RUN export CARGO_HOME=/tmp/.cargo && \
    makepkg -c && \
    source $(pwd)/PKGBUILD && \
    mv "${pkgname}"-"${pkgver}"-"${pkgrel}"-"$(uname -m)".pkg.tar.zst dst.pkg.tar.zst

FROM archlinux:base-devel AS builder
COPY --from=build /tmp/dst.pkg.tar.zst /tmp/dst.pkg.tar.zst
RUN pacman -Syu git --noconfirm && \
    pacman -U /tmp/dst.pkg.tar.zst --noconfirm && \
    rm -rf /var/cache/pacman/pkg/* && \
    rm /tmp/dst.pkg.tar.zst && \
    echo "nobody ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

WORKDIR /home/nobody
COPY ./build-package.sh ./build-package.sh
RUN mkdir build db files && \
    chown nobody:nobody -R /home/nobody && \
    chmod 770 /home/nobody && \
    usermod nobody -d /home/nobody
RUN chmod u+x ./build-package.sh

USER nobody
