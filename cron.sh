#!/bin/bash

WORKDIR="$(dirname "$0")"
cd "$WORKDIR" || exit 1

test -e "$WORKDIR/build" || mkdir "$WORKDIR/build"
touch "$WORKDIR/build/$REPO_NAME.db.tar.gz"

FILE=""

if [[ $UID != 0 ]]; then
    echo "Need Root"
    exit 1
fi

case "$1" in
"hourly")
    FILE="packages/hourly"
;;

"daily")
    FILE="packages/daily"
;;

"weekly")
    FILE="packages/weekly"
;;

*)
echo "No time given"
exit 1
esac

CONTENT="$(cat $FILE)"

for LINE in $CONTENT; do
    docker run --rm \
        -e "REPO_NAME=$REPO_NAME" \
        -v "$(pwd)/build":"/home/nobody/build" \
        -v "$(pwd)/signing.asc":"/home/nobody/signing.asc":ro \
        peek1e/archlinux:aur-builder "/bin/bash" "/home/nobody/build-package.sh" "$LINE"
done

chown 65534:www-data -R "$(pwd)/build"
chmod 770 -R "$WORKDIR/build"

exit 0
