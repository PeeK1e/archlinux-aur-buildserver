#!/bin/bash

if [[ -z $1 ]]; then
    echo "No package Provided"
    exit 1
fi

echo "Checking existence of target package"

git clone "https://aur.archlinux.org/$1.git" pkg || exit 1

source "$HOME/pkg/PKGBUILD" || exit 1

ARCH="$(uname -m)"

if [[ "${arch[0]}" == "any" ]]; then
    ARCH="any"
fi

if sudo test -e "$HOME/build/${pkgname}-${pkgver}-${ARCH}-.pkg.tar.zst"; then
    echo "Package ${pkgname} in version ${pkgver}-${pkgrel} exists"
    exit 0
fi

gpg --allow-secret-key-import --import "$HOME/signing.asc"

# Install prerequisites
paru -Sy --noconfirm --needed --asdeps "${makedepends[@]}" "${depends[@]}"
cd "$HOME/pkg" || exit 1
makepkg --sign || exit 1

cd "$HOME" || exit 1

# Replace package
sudo find '$HOME/build/' -name "${pkgname}-*-*-${ARCH}.pkg.*" -delete
sudo repo-remove "$HOME/build/$REPO_NAME.db.tar.gz" "${pkgname}"

cd "$HOME/pkg" || exit 1
sudo find './' -name "${pkgname}-${pkgver}-${pkgrel}-${ARCH}.pkg.*" -exec sudo cp {} ../build/{} \;
sudo repo-add --verify --sign "$HOME/build/$REPO_NAME.db.tar.gz" "$HOME/pkg/${pkgname}-${pkgver}-${pkgrel}-${ARCH}.pkg.tar.zst"

exit 0
